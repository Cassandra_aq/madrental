package com.example.madrental.bdd;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {VehiculeDTO.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase
{
    public abstract VehiculesDAO vehiculesDAO();
}
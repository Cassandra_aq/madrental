package com.example.madrental.bdd;

import android.net.Uri;
import android.util.Log;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

@Entity(tableName = "vehicules")
public class VehiculeDTO {

    @PrimaryKey(autoGenerate = true)
    public long id = 0;
    public String nom;
    // @TypeConverters(UriConverters.class)
    public String image;
    public String categorieco2;
    public int disponible;
    public int prixjournalierbase;
    public int promotion;
    public int ageMin;

    // Constructeur public vide (obligatoire si autre constructeur existant) :
    public VehiculeDTO() {}

    // Autre constructeur :
    public VehiculeDTO(int id, String nom, String image, String categorieco2, int disponible, int prixjournalierbase, int promotion, int ageMin)
    {
        this.id = id;
        this.nom = nom;
        this.image = image;
        this.categorieco2 = categorieco2;
        this.disponible = disponible;
        this.prixjournalierbase = prixjournalierbase;
        this.promotion = promotion;
        this.ageMin = ageMin;
    }
}

package com.example.madrental.bdd;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public abstract class VehiculesDAO {

    @Query("SELECT * FROM vehicules")
    public abstract List<VehiculeDTO> getListeVehicules();

    @Insert
    public abstract void insert(VehiculeDTO... vehicules);

    @Update
    public abstract void update(VehiculeDTO... vehicules);

    @Delete
    public abstract void delete(VehiculeDTO... vehicules);

    @Query("DELETE FROM vehicules")
    public abstract void deleteAll();

    @Query("SELECT Count(*) FROM vehicules WHERE id = :vehiculeID")
    public abstract int countVehiculeById(int vehiculeID);

    @Query("SELECT * FROM vehicules WHERE id = :vehiculeID")
    public abstract VehiculeDTO getVehiculeById(int vehiculeID);
}

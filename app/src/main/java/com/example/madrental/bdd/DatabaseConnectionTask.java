package com.example.madrental.bdd;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.madrental.helpers.AppDatabaseHelper;

public class DatabaseConnectionTask extends AsyncTask<Context, Integer, Long> {

    @Override
    protected Long doInBackground(Context... contexts) {

        for (Context context : contexts) {
            AppDatabaseHelper.getDatabase(context);
            // Escape early if cancel() is called
            if (isCancelled()) break;
        }

        return null;
    }

    protected void onPostExecute(Long result) {
        Log.i("lul", "connecté à la BDD");
    }
}

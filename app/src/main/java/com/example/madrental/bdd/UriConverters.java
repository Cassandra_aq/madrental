package com.example.madrental.bdd;

import android.net.Uri;

import androidx.room.TypeConverter;

class UriConverters {
    @TypeConverter
    public Uri fromString(String imageUrl) {
        if (imageUrl == null) {
            return null;
        }
        return Uri.parse(imageUrl);
    }

    @TypeConverter
    public String toString(Uri imageUrl) {
        if (imageUrl == null) {
            return null;
        }
        return imageUrl.toString();
    }
}

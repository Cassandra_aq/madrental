package com.example.madrental.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.example.madrental.R;
import com.example.madrental.models.Vehicule;
import com.squareup.picasso.Picasso;

public class DetailFragment extends Fragment {

    public static final String KEY_NOM = "nom";
    public static final String KEY_IMAGE = "image";
    public static final String KEY_CAT = "categorieCo2";
    public static final String KEY_DISP = "disponible";
    public static final String KEY_PRIXJR = "prixJournalierBase";
    public static final String KEY_PROMO = "promotion";
    public static final String KEY_AGE = "ageMin";

    public int id;
    public String nom;
    public String image;
    public String categorieCo2;
    public int disponible;
    public String prixJournalierBase;
    public int promotion;
    public int ageMin;
    public boolean switchIsChecked;

    public DetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            nom = getArguments().getString(KEY_NOM);
            image = Vehicule.siteImage + getArguments().getString(KEY_IMAGE);
            categorieCo2 = getArguments().getString(KEY_CAT);
            disponible = getArguments().getInt(KEY_DISP);
            int prix = getArguments().getInt(KEY_PRIXJR);
            promotion = getArguments().getInt(KEY_PROMO);
            if (promotion > 0) {
                prixJournalierBase = (prix - promotion) + "€ / jour (promo -" + promotion + "€ !)";
            } else {
                prixJournalierBase = prix + "€ / jour";
            }
            ageMin = getArguments().getInt(KEY_AGE);
            int isChecked = getArguments().getInt("isChecked");
            switchIsChecked = isChecked == 1;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detail,
                container,
                false);
        ImageView imageView = view.findViewById(R.id.image_vehicule);
        TextView textNom = view.findViewById(R.id.nom_vehicule);
        TextView textPrix = view.findViewById(R.id.prix_vehicule);
        TextView textCat = view.findViewById(R.id.categorie_vehicule);
        Switch ajoutFavoris = view.findViewById(R.id.ajout_favoris);

        ajoutFavoris.setChecked(switchIsChecked);

        // affichage des données de la voiture
        Picasso.with(view.getContext())
                .load(image)
                .fit()
                .centerCrop()
                .into(imageView);
        textNom.setText(nom);
        textPrix.setText(prixJournalierBase);
        textCat.setText(categorieCo2);

        return view;
    }
}
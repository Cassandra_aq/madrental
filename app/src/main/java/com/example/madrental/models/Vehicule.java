package com.example.madrental.models;

import java.io.Serializable;

public class Vehicule implements Serializable {

    public static String siteImage = "http://s519716619.onlinehome.fr/exchange/madrental/images/";

    public int id;
    public String nom;
    public String imageUrl;
    public String categorieCo2;
    public int disponible;
    public int prixJournalierBase;
    public int promotion;
    public int ageMin;


    public Vehicule(int id, String nom, String imageUrl, String categorieCo2, int disponible, int prixJournalierBase, int promotion, int ageMin) {
        this.id = id;
        this.nom = nom;
        this.imageUrl = imageUrl;
        this.disponible = disponible;
        this.prixJournalierBase = prixJournalierBase;
        this.promotion = promotion;
        this.ageMin = ageMin;
        this.categorieCo2 = categorieCo2;
    }
}

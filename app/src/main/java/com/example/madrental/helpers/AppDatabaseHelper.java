package com.example.madrental.helpers;

import android.content.Context;

import androidx.room.Room;

import com.example.madrental.bdd.AppDatabase;

public class AppDatabaseHelper {
    // Attributs :
    private static AppDatabaseHelper databaseHelper = null;
    private AppDatabase database;

    // Constructeur :
    private AppDatabaseHelper(Context context) {
        database = Room
                .databaseBuilder(context, AppDatabase.class, "madRental.db")
                .allowMainThreadQueries()
                .build();
    }

    // Getter instance :
    public static synchronized AppDatabase getDatabase(Context context) {
        if (databaseHelper == null) {
            databaseHelper = new AppDatabaseHelper(
                    context.getApplicationContext());
        }
        return databaseHelper.database;
    }
}

package com.example.madrental.helpers;

import android.util.Log;

import com.example.madrental.activites.MainActivity;
import com.example.madrental.bdd.VehiculeDTO;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;


import java.util.Arrays;

import cz.msebera.android.httpclient.Header;

public class WebServiceHelper {

    public void get(String WsUrl, final MainActivity activity) {

        // client HTTP :
        AsyncHttpClient client = new AsyncHttpClient();

        // appel :
        client.get(WsUrl, new AsyncHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String retour = new String(responseBody);
                Gson gson = new Gson();
                Log.i("lul", retour);
                VehiculeDTO[] listeVehiculesDTO = gson.fromJson(retour, VehiculeDTO[].class);
                activity.wsCallback(listeVehiculesDTO);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] response, Throwable error) {
                Log.e("failure", Arrays.toString(response));
            }
        });
    }
}

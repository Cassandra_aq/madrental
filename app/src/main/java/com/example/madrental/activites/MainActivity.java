package com.example.madrental.activites;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Switch;

import com.example.madrental.R;
import com.example.madrental.adapters.VehiculesAdapter;
import com.example.madrental.bdd.DatabaseConnectionTask;
import com.example.madrental.fragments.DetailFragment;
import com.example.madrental.helpers.AppDatabaseHelper;
import com.example.madrental.bdd.VehiculeDTO;
import com.example.madrental.helpers.WebServiceHelper;
import com.example.madrental.models.Vehicule;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private FrameLayout conteneurFragmentDetail = null;

    VehiculesAdapter vehiculesAdapter;
    RecyclerView recyclerView;
    ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinner = (ProgressBar)findViewById(R.id.progressBar1);
        spinner.setVisibility(View.VISIBLE);

        conteneurFragmentDetail = findViewById(R.id.conteneur_detail);

        // start database
        new DatabaseConnectionTask().execute(this);

        // RecyclerView
        recyclerView = findViewById(R.id.liste_vehicules);
        recyclerView.setHasFixedSize(true);

        // layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        // populate recyclerview with empty list while waiting for WS return
        List<VehiculeDTO> listeVehiculesDTO = new ArrayList<>();
        updateRecyclerView(listeVehiculesDTO);

        // display list of vehicules from WS or favorites depending on switch value
        final Switch afficherFavoris = findViewById(R.id.afficher_favoris);
        displayFavOrWs(afficherFavoris.isChecked());
        afficherFavoris.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                spinner.setVisibility(View.VISIBLE);
                displayFavOrWs(afficherFavoris.isChecked());
            }
        });
    }

    public void getWsVehicules() {
        WebServiceHelper webServiceHelper = new WebServiceHelper();
        webServiceHelper.get("http://s519716619.onlinehome.fr/exchange/madrental/get-vehicules.php", this);
    }

    public void wsCallback(VehiculeDTO[] listeVehiculesDTO) {
        updateRecyclerView(Arrays.asList(listeVehiculesDTO));

        spinner.setVisibility(View.GONE);
    }

    public void updateRecyclerView(List<VehiculeDTO> listeVehiculesDTO) {

        List<Vehicule> listeVehicules = new ArrayList<>();

        for (VehiculeDTO vehicule : listeVehiculesDTO) {
            listeVehicules.add(new Vehicule((int) vehicule.id, vehicule.nom, vehicule.image, vehicule.categorieco2, vehicule.disponible, vehicule.prixjournalierbase, vehicule.promotion, vehicule.ageMin));
        }

        // adapter
        vehiculesAdapter = new VehiculesAdapter(listeVehicules, this);
        recyclerView.setAdapter(vehiculesAdapter);
    }

    public void onClicItem(int position)
    {
        // récupération du véhicule à cette position :
        Vehicule vehicule = vehiculesAdapter.getItemParPosition(position);

        // affichage du détail :
        if (conteneurFragmentDetail != null)
        {
            // fragment :
            DetailFragment fragment = new DetailFragment();
            Bundle bundle = new Bundle();
            bundle.putString(DetailFragment.KEY_NOM, vehicule.nom);
            bundle.putString(DetailFragment.KEY_IMAGE, vehicule.imageUrl);
            bundle.putString(DetailFragment.KEY_CAT, vehicule.categorieCo2);
            bundle.putInt(DetailFragment.KEY_DISP, vehicule.disponible);
            bundle.putInt(DetailFragment.KEY_PRIXJR, vehicule.prixJournalierBase);
            bundle.putInt(DetailFragment.KEY_PROMO, vehicule.promotion);
            bundle.putInt(DetailFragment.KEY_AGE, vehicule.ageMin);

            fragment.setArguments(bundle);

            // le conteneur de la partie détail est disponible, on est donc en mode "tablette" :
            getSupportFragmentManager().beginTransaction().replace(R.id.conteneur_detail, fragment).commit();
        }
        else
        {
            // le conteneur de la partie détail n'est pas disponible, on est donc en mode "smartphone" :
            Intent intent = new Intent(this, DetailActivity.class);

            intent.putExtra("Vehicule", vehicule);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
        }
    }

    public void displayFavOrWs(boolean isChecked) {
        if (isChecked) {
            // switch button is checked
            // display vehicules from favorites
            List<VehiculeDTO> listeVehiculesDTO = AppDatabaseHelper.getDatabase(this).vehiculesDAO().getListeVehicules();
            updateRecyclerView(listeVehiculesDTO);
            spinner.setVisibility(View.GONE);
        } else {
            // switch button is unchecked (default)
            // display vehicules from webservice
            getWsVehicules();
        }
    }

}
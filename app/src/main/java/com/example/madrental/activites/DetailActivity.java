package com.example.madrental.activites;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.madrental.R;
import com.example.madrental.bdd.VehiculeDTO;
import com.example.madrental.fragments.DetailFragment;
import com.example.madrental.helpers.AppDatabaseHelper;
import com.example.madrental.models.Vehicule;

public class DetailActivity extends AppCompatActivity {

    Vehicule vehicule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        vehicule = (Vehicule) getIntent().getSerializableExtra("Vehicule");

        // fragment :
        DetailFragment fragment = new DetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString("nom", vehicule.nom);
        bundle.putString("image", vehicule.imageUrl);
        bundle.putString("categorieCo2", vehicule.categorieCo2);
        bundle.putInt("disponible", vehicule.disponible);
        bundle.putInt("prixJournalierBase", vehicule.prixJournalierBase);
        bundle.putInt("promotion", vehicule.promotion);
        bundle.putInt("ageMin", vehicule.ageMin);
        bundle.putInt("isChecked", isChecked(vehicule));

        fragment.setArguments(bundle);
        // fragment manager :
        FragmentManager fragmentManager = getSupportFragmentManager();
        // transaction :
        FragmentTransaction fragmentTransaction = ((FragmentManager) fragmentManager).beginTransaction();
        fragmentTransaction.replace(R.id.conteneur_fragment, fragment, "detailFragment");
        fragmentTransaction.commit();
    }

    public void ajoutFavoris(View view)   {

        // Vérification de la présence du véhicule en BDD
        int nbVehicule = AppDatabaseHelper.getDatabase(this).vehiculesDAO().countVehiculeById(vehicule.id);
        VehiculeDTO vehiculeDTO = new VehiculeDTO(vehicule.id, vehicule.nom, vehicule.imageUrl, vehicule.categorieCo2, vehicule.disponible, vehicule.prixJournalierBase, vehicule.promotion, vehicule.ageMin);

        if (nbVehicule == 0){
            AppDatabaseHelper.getDatabase(this).vehiculesDAO().insert(vehiculeDTO);

            Toast.makeText(view.getContext(),"Véhicule ajouté à vos favoris", Toast.LENGTH_SHORT).show();
        }
        // sinon on supprime de la BDD
        else{
            AppDatabaseHelper.getDatabase(this).vehiculesDAO().delete(vehiculeDTO);

            Toast.makeText(view.getContext(),"Véhicule supprimé de vos favoris", Toast.LENGTH_SHORT).show();
        }

    }

    public int isChecked(Vehicule vehicule) {
        VehiculeDTO vehiculeDTO = AppDatabaseHelper.getDatabase(this).vehiculesDAO().getVehiculeById(vehicule.id);

        if(vehiculeDTO != null) {
            return 1;
        }
        return 0;
    }
}
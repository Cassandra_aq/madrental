package com.example.madrental.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.madrental.R;
import com.example.madrental.activites.MainActivity;
import com.example.madrental.models.Vehicule;
import com.squareup.picasso.Picasso;

import java.util.List;

public class VehiculesAdapter extends RecyclerView.Adapter<VehiculesAdapter.VehiculeViewHolder>
{
    // Liste d'objets métier :
    private List<Vehicule> listeVehicules;
    private MainActivity mainActivity = null;

    // Constructeur :
    public VehiculesAdapter(List<Vehicule> listeVehicules, MainActivity mainActivity)
    {
        this.mainActivity = mainActivity;
        this.listeVehicules = listeVehicules;
    }

    @NonNull
    @Override
    public VehiculeViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View viewVehicule = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.vehicule_item_liste, parent, false);

        return new VehiculeViewHolder(viewVehicule);
    }

    @Override
    public void onBindViewHolder(VehiculeViewHolder holder, int position)
    {
        String prixJournalierBase = listeVehicules.get(position).prixJournalierBase + "€ / jour";
        holder.textViewNomVehicule.setText(listeVehicules.get(position).nom);
        holder.textViewPrixVehicule.setText(prixJournalierBase);
        holder.textViewCategorieVehicule.setText(listeVehicules.get(position).categorieCo2);
        if (listeVehicules.get(position).imageUrl != null) {
            String imageVehiculeURI = Vehicule.siteImage + listeVehicules.get(position).imageUrl;
            Picasso.with(holder.imageViewVehicule.getContext())
                    .load(imageVehiculeURI)
                    .fit()
                    .centerCrop()
                    .into(holder.imageViewVehicule);
        } else {
            Log.i("lul", "no image URI");
        }
    }

    // récupération de la voiture correspondant à une position
    public Vehicule getItemParPosition(int position){
        return listeVehicules.get(position);
    }

    @Override
    public int getItemCount()
    {
        return listeVehicules.size();
    }

    public class VehiculeViewHolder extends RecyclerView.ViewHolder {

        // TextView contenu vehicule :
        public TextView textViewNomVehicule;
        public TextView textViewPrixVehicule;
        public TextView textViewCategorieVehicule;
        public ImageView imageViewVehicule;

        // Constructeur :
        public VehiculeViewHolder(final View itemView) {
            super(itemView);
            textViewNomVehicule = itemView.findViewById(R.id.nom_vehicule);
            textViewPrixVehicule = itemView.findViewById(R.id.prix_vehicule);
            textViewCategorieVehicule = itemView.findViewById(R.id.categorie_vehicule);
            imageViewVehicule = itemView.findViewById(R.id.image_vehicule);

            itemView.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    mainActivity.onClicItem(getAdapterPosition());
                }
            });
        }

    }
}